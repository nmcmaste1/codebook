let k, ol, hs

const randint = limit => Math.floor(Math.random() * limit)
const rx = () => randint(game.config.width)
const ry = () => randint(game.config.height)

class Main extends Phaser.Scene {

    init() {
        hs = localStorage.getItem('hs')
    }

    preload() {
        this.load.image('bg', 'assets/img/background.png')
        this.load.image('01', 'assets/img/kenny.png')
        this.load.image('02', 'assets/img/kim-jon-un.png')
        this.load.image('de', 'assets/img/game-over.png')
        this.load.audio('ed', 'assets/snd/gameover.wav')
        this.load.image('dr', 'assets/img/drphil.png')
        this.load.image('03', 'assets/img/deformed_spongebob.png') 
 
        this.load.image(`la`, `assets/img/lava.png`)
        this.load.image(`yo`, `assets/img/flame_thrower.png`)
        this.load.image(`y1`, `assets/img/uzi.png`)
        this.load.image(`y3`, `assets/img/hover-scooter.png`)
        this.load.image(`y4`, `assets/img/hover-scooter_clone_power-up.png`)


    }

    create() {
        this.add.image(0, 0, 'bg').setOrigin(0, 0)
        let score = 0
        let scoreText = this.add.text(900, 100, `Score:${score}`)
        let hscoreText = this.add.text(900, 150, `High:${hs}`)
        // score = score + 1
        // score += 1
        // score++

        const spawnPower = () => {

        }

        setInterval(() => {
            score = score + 1
            scoreText.setText(`Score:${score}`)
            if (score > hs) {
                hs = score
                localStorage.setItem(`hs`, score)
                hscoreText.setText(`High:${hs}`)
            }
        }, 1000)

        let phil = this.physics.add.sprite(250, 500, 'dr')
        phil.setScale(20)
        phil.setVelocity(50)
        phil.setCollideWorldBounds(true)
        phil.setBounce(1.001)

        phil.setImmovable();
        ol = this.physics.add.sprite(80, 600, '01')
        ol.setGravityY(200)
        ol.setCollideWorldBounds(true)
        k = this.input.keyboard.addKeys('LEFT,RIGHT,UP,DOWN,W,A,D,S,R')
        
        let gameover = this.sound.add("ed")
        
        //--------------------------------------
        
        let enemies = this.physics.add.group()
        const spawnEnemy = (x, y) => {
            let e = enemies.create(rx(), ry(), "02")
            e.setVelocity(500)
            e.setCollideWorldBounds(true)
            e.setBounce(1)
            e.setGravityY(800)
        }
        spawnEnemy(500, 200)
        let enemyspawn = setInterval(spawnEnemy, 5000)
        
        //--------------------------------------
        let power_ups= this.physics.add.group()
        const spawnPowerUp = (x, y) => {
            let p = power_ups.create(rx(), ry(), "y4")
            p.setVelocity(0)
            p.setCollideWorldBounds(true)
            p.setBounce(0)
            p.setGravityY(0)
            p.setScale(5)
        }
        let powerupspawn = setInterval(spawnPowerUp, 5000)

        const recieve_power_up = (p,e) => {
            p.setVelocity(5000)

            e.destroy()
        }
        //--------------------------------------

        let lasers = this.physics.add.group()
        const spawnbob = () => {
            let l = lasers.create(rx(), ry(), "03")
            l.setVelocity(500)
            l.setCollideWorldBounds(true)
            l.setBounce(1)
            l.setGravityY(20)
            l.setScale(3)

        }

        let bobspawn = setInterval(spawnbob, 7000)



        const death = (p, e) => {
            clearInterval(enemyspawn)
            clearInterval(bobspawn)
            this.physics.pause()
            this.add.image(0, 0, 'de').setOrigin(0, 0)
            gameover.play()
        }

        const killboth = (l, e) => {
            l.destroy()
            e.destroy()
        }
        this.physics.add.collider(ol,power_ups,recieve_power_up)

        this.physics.add.collider(ol, enemies, death)
        this.physics.add.collider(lasers, enemies, killboth)
        //this.physics.add.collider(phil, enemies)
    } 
      
    update() {
        if (k.LEFT.isDown) {
            ol.setVelocityX(-400)
        }
        if (k.RIGHT.isDown) {
            ol.setVelocityX(400)
        }
        if (k.UP.isDown) {
            ol.setVelocityY(-400)
        }
        if (k.R.isDown) {
            this.scene.restart()
        }
        if (k.DOWN.isDown) {
            ol.setVelocityY(400)
        }

    }

}

let game = new Phaser.Game({
    scene: Main,
    physics: { default: 'arcade' },
    pixelArt: true,
    scale: Phaser.Scale.FIT,
    autoCenter: Phaser.Scale.CENTER_BOTH, 
})